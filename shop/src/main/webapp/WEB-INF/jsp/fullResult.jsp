<!doctype html/>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title> Full result page </title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css" rel="stylesheet">
</head>

<body>
<div id="content">
    <table>
        <thead>
        <tr>
            <th>Количество мужчин</th>
            <th>Кол-во женшин</th>
            <th>Кол-во детей</th>
            <th>Кол-во людей в очередях</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="step" items="${listOfSteps}">
            <tr>
                <td>${step.getCurMenQty()}</td>
                <td>${step.getCurWomenQty()}</td>
                <td>${step.getCurChildrenQty()}</td>
                <td>
                    <c:forEach var="cashDesk" items="${step.getCashDesksList()}">

                        ${cashDesk.getQueueLength()}
                    </c:forEach>

                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>

</body>

</html>
