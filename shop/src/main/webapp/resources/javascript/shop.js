$(function () {

    var options = {
        success: success,       // post-submit callback
        dataType: 'json'
    };
    $('#postButton').click(function () {
        $('.listitems').remove();
        var posting = $.post('getJson', $('form').serialize());
        posting.done(success);
    });

    $('#shopForm').submit(function () {
        $(this).ajaxSubmit(options);
        return false;
    });
    $('#ajaxButton').click(function () {
        $('.listitems').remove();
        $.ajax({
            type: 'POST',
            url: 'getJson',
            data: $('form').serialize(),
            dataType: 'json',
            success: success
        });
    });
});

function success(data) {

    var items = [];
    $.each(data, function (i, item) {
        items.push('<li class="listitems">' + 'Количество мужчин: ' + item.curMenQty + 'Количество женищин ' + item.curWomenQty +
        'Количество детей ' + item.curChildrenQty);
        items.push('<ol>');
        $.each(item.cashDesksList, function (i, cashdesk) {
            items.push('<li>' + '<div class="row">' + '<ul>');
            items.push('<li>' + '<div class="cashDesk">' +
            '<img src="' + context + '/resources/images/cashDesk.jpg" />' +
            '<span>' + cashdesk.velocity + '</span>' + '</div>' + '</li>');
            $.each(cashdesk.customersQueue, function (i, customer) {
                if (customer.sex == "M") {
                    items.push('<li>' + '<div class="men">' +
                    '<img src= "' + context + '/resources/images/man.jpg" />' + '<span>' + customer.productQty + '</span>' +
                    '</div>' + '</li>');
                } else if (customer.sex == "W") {
                    items.push('<li>' + '<div class="woman">' +
                    '<img src= "' + context + '/resources/images/woman.jpg" />' + '<span>' + customer.productQty + '</span>' +
                    '</div>' + '</li>');
                } else {
                    items.push('<li>' + '<div class="child">' +
                    '<img src= "' + context + '/resources/images/child.jpg" />' + '<span>' + customer.productQty + '</span>' +
                    '</div>' + '</li>');
                }
            });
            items.push('</ul>' + '</div>' + '</li>');
        });
        items.push('</ol>' + '</li>');
    });
    $('#listitems').append(items.join(''));
    $('.pagination').show();
    $('.listitems').hide();
    $($('.listitems')[0]).show();
    $('.pagination').jqPagination({
        max_page: $('.listitems').length,
        paged: function (page) {
            $('.listitems').hide();
            $($('.listitems')[page - 1]).show();
        }
    });
}
