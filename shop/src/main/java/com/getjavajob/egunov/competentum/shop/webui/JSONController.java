package com.getjavajob.egunov.competentum.shop.webui;

import com.getjavajob.egunov.competentum.shop.entities.Shop;
import com.getjavajob.egunov.competentum.shop.entities.Step;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class JSONController {

    @RequestMapping(value = "/MainPage")
    public String getMainPage() {
        return "mainPage";
    }

    @RequestMapping(value = "/getSteps", method = RequestMethod.POST)
    public ModelAndView getStepsWithoutAjax(@ModelAttribute ShopForm shopForm) {
        Shop shop = new Shop(shopForm.getCashDeskQty(), shopForm.getProductQtyRange(), shopForm.getMenPercentage(),
                shopForm.getWomenPercentage(), shopForm.getReqStepsQty(), shopForm.getCashDeskVelocity());
        ModelAndView result = new ModelAndView("fullResult").addObject("listOfSteps", shop.getListOfSteps());

        return result;
    }

    @RequestMapping(value = "/getJson", method = RequestMethod.POST)
    public
    @ResponseBody
    List<Step> getStepsWithAjax(@ModelAttribute ShopForm shopForm) {
        Shop shop = new Shop(shopForm.getCashDeskQty(), shopForm.getProductQtyRange(), shopForm.getMenPercentage(),
                shopForm.getWomenPercentage(), shopForm.getReqStepsQty(), shopForm.getCashDeskVelocity());
        List<Step> result = shop.getListOfSteps();
        return result;
    }
}
