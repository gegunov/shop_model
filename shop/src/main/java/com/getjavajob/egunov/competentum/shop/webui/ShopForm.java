package com.getjavajob.egunov.competentum.shop.webui;

public class ShopForm {

    private int cashDeskQty;
    private int menPercentage;
    private int womenPercentage;
    private int reqStepsQty;
    private int productQtyRange;
    private int cashDeskVelocity;

    public int getMenPercentage() {
        return menPercentage;
    }

    public void setMenPercentage(int menPercentage) {
        this.menPercentage = menPercentage;
    }

    public int getWomenPercentage() {
        return womenPercentage;
    }

    public void setWomenPercentage(int womenPercentage) {
        this.womenPercentage = womenPercentage;
    }

    public int getReqStepsQty() {
        return reqStepsQty;
    }

    public void setReqStepsQty(int reqStepsQty) {
        this.reqStepsQty = reqStepsQty;
    }

    public int getCashDeskQty() {
        return cashDeskQty;
    }

    public void setCashDeskQty(int cashDeskQty) {
        this.cashDeskQty = cashDeskQty;
    }

    public int getProductQtyRange() {
        return productQtyRange;
    }

    public void setProductQtyRange(int productQtyRange) {
        this.productQtyRange = productQtyRange;
    }

    public int getCashDeskVelocity() {
        return cashDeskVelocity;
    }

    public void setCashDeskVelocity(int cashDeskVelocity) {
        this.cashDeskVelocity = cashDeskVelocity;
    }
}
