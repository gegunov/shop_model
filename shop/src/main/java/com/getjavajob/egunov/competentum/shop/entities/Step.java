package com.getjavajob.egunov.competentum.shop.entities;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Step {

    private int curMenQty;
    private int curWomenQty;
    private int curChildrenQty;
    private int reqStepsQty;
    @JsonProperty
    private List<CashDesk> cashDesksList;

    public Step(int curMenQty, int curWomenQty, int curChildrenQty, List<CashDesk> cashDesksList) {
        this.curMenQty = curMenQty;
        this.curWomenQty = curWomenQty;
        this.curChildrenQty = curChildrenQty;
        this.cashDesksList = cashDesksList;
    }

    public int getCurMenQty() {
        return curMenQty;
    }

    public void setCurMenQty(int curMenQty) {
        this.curMenQty = curMenQty;
    }

    public int getCurWomenQty() {
        return curWomenQty;
    }

    public void setCurWomenQty(int curWomenQty) {
        this.curWomenQty = curWomenQty;
    }

    public int getCurChildrenQty() {
        return curChildrenQty;
    }

    public void setCurChildrenQty(int curChildrenQty) {
        this.curChildrenQty = curChildrenQty;
    }

    public int getReqStepsQty() {
        return reqStepsQty;
    }

    public void setReqStepsQty(int reqStepsQty) {
        this.reqStepsQty = reqStepsQty;
    }

    public List<CashDesk> getCashDesksList() {
        return cashDesksList;
    }

    public void setCashDesksList(List<CashDesk> cashDesksList) {
        this.cashDesksList = cashDesksList;
    }
}
