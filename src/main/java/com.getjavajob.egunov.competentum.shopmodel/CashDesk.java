package com.getjavajob.egunov.competentum.shopmodel;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Created by Администратор on 17.01.2015.
 */
public class CashDesk {

    private Deque<Customer> queueOfCostumers = new ConcurrentLinkedDeque<>();
    private int capacity;

    public CashDesk(int capacity){
        this.capacity = capacity;
    }

    public void addBuyerToQueue(Customer customer){
        queueOfCostumers.addFirst(customer);
    }

    public int countStepsToLastCustomer(){
        int steps = 0;
        for(Customer customer : queueOfCostumers){
            steps += (customer.getProductQuantity() % capacity == 0) ? customer.getProductQuantity() % capacity :
                    customer.getProductQuantity() % capacity + 1;
        }
        return steps;
    }

    public int getQueueLength(){
        return queueOfCostumers.size();
    }

    public void serve(){
        if(queueOfCostumers.size() != 0) {
            Customer currentCustomer = queueOfCostumers.getLast();
            if (currentCustomer.getProductQuantity() > 0) {
                int customerProductQuantity = queueOfCostumers.getLast().getProductQuantity();
                currentCustomer.setProductQuantity(customerProductQuantity - capacity);
            } else {
                queueOfCostumers.removeLast(); //Это действие эквивалентно рассчёту с покупателем в обычной жизни
                //переход к следующему покупателю будет выполнен уже в другой шаг.
            }
        }
    }





}
