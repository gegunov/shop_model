package com.getjavajob.egunov.competentum.shopmodel;

import java.util.List;
import java.util.Random;

public class Child extends Customer {

    public Child(int productQuantity) {
        super(productQuantity);
    }

    @Override
    public void chooseCashDesk(List<CashDesk> cashDeskList) {
        Random random = new Random();
        int actualCashDeskIndex = random.nextInt(cashDeskList.size());
        cashDeskList.get(actualCashDeskIndex).addBuyerToQueue(this);
    }
}
