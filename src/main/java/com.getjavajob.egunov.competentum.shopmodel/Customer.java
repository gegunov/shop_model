package com.getjavajob.egunov.competentum.shopmodel;

import java.util.List;

public abstract class Customer {

    private int productQuantity;

    public Customer(int productQuantity){
        this.productQuantity = productQuantity;
    }


    public void chooseCashDesk(List<CashDesk> cashDeskList){

    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

}
