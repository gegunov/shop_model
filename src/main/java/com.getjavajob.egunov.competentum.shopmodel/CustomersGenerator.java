package com.getjavajob.egunov.competentum.shopmodel;

import java.util.Random;

public class CustomersGenerator {

    int menPercentage;
    int womenPercentage;
    int childrenPercentage;
    int productQuantityRange;
    Random random;

    public CustomersGenerator(int menPercentage, int womenPercentage, int productQuantityRange){
        this.menPercentage = menPercentage;
        this.womenPercentage = womenPercentage;
        this.childrenPercentage = 100 - (menPercentage + womenPercentage);
        this.productQuantityRange = productQuantityRange;
        random = new Random();
    }
    public Customer getNewCustomer(){
        int sexDeterminant = random.nextInt(101);
        int switchVariable = 2;
        if (sexDeterminant <= menPercentage){
            switchVariable = 0; // Мужчина
        }else if(switchVariable > menPercentage && switchVariable <= (menPercentage + womenPercentage)){
            switchVariable = 1; // Женщина
        }

        switch (switchVariable){
            case 0:
                return new Men(random.nextInt(productQuantityRange));

            case 1:
                return new Woman(random.nextInt(productQuantityRange));

            default:
                return new Child(productQuantityRange);
        }
    }
}
