package com.getjavajob.egunov.competentum.shopmodel;

import java.util.List;

public class Woman extends Customer {

    public Woman(int productQuantity) {
        super(productQuantity);
    }

    @Override
    public void chooseCashDesk(List<CashDesk> cashDeskList) {
        CashDesk actual = cashDeskList.get(0);
        for(CashDesk cashDesk : cashDeskList){
            if(cashDesk.getQueueLength() < actual.getQueueLength()){
                actual = cashDesk;
            }
        }
        actual.addBuyerToQueue(this);
    }
}
