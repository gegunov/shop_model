package com.getjavajob.egunov.competentum.shopmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TradeChamber {

    private List<CashDesk> cashDeskList;
    private CustomersGenerator customersGenerator;
    private Random random;
    private int countSteps = 0;

    public TradeChamber(int cashDeskQuantity, int productQuantityRange, int menPercentage, int womenPercentage){
        random = new Random();
        customersGenerator = new CustomersGenerator(menPercentage, womenPercentage, productQuantityRange);
        cashDeskList = new ArrayList<>(cashDeskQuantity);
        for(int i = 0; i < cashDeskQuantity; i++){
            CashDesk cashDesk = new CashDesk(random.nextInt(10)+1);
            cashDeskList.add(cashDesk);
        }
    }

    public void getOneStep(){
        createNewCustomer();
        for(CashDesk cashDesk : cashDeskList){
            cashDesk.serve();
        }
        System.out.printf("It's %d step!", ++countSteps );
    }

    public List<CashDesk> getCashDeskList(){
        return cashDeskList;
    }

    public void createNewCustomer(){
        Customer customer = customersGenerator.getNewCustomer();
        customer.chooseCashDesk(cashDeskList);
    }



}
