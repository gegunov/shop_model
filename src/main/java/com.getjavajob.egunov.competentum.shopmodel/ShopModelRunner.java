package com.getjavajob.egunov.competentum.shopmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShopModelRunner {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String [] args){
        List<Integer> percents = getCustomerPercentage();
        TradeChamber tradeChamber = new TradeChamber(aakForProductQuantityRange(),askForCashDeskQuantity(),percents.get(0),percents.get(1));
        int quantityOfSteps = 10;
        System.out.println("Enter quantity of steps");
        try {
            quantityOfSteps = scanner.nextInt();
        }catch (NumberFormatException e){
            System.out.println("Wrong number! Quantity of steps will set to 10");
        }
        while (quantityOfSteps > 0){
            tradeChamber.getOneStep();
        }
    }


    public static int aakForProductQuantityRange(){
        System.out.println("Enter max product quantity");
        int result = 30;
        try{
            result = scanner.nextInt();
        }catch (NumberFormatException e){
            System.out.println("Wrong number! Product range will set to 30");
        }
        return result;

    }

    public static List<Integer> getCustomerPercentage(){
        Integer menPercentage = 33;
        Integer womenPercentage = 33;
        List<Integer> result = new ArrayList<>(2);
        System.out.println("Enter men percentage (0-100)");
        try {
            menPercentage = scanner.nextInt();
            if(menPercentage < 0 || menPercentage > 100){
                System.out.println("Wrong quantity! The men's percentage will set to 33");
                menPercentage = 33;
            }
        }catch (NumberFormatException e){
            System.out.println("Wrong quantity! The men's percentage will set to 33");
        }
        result.add(menPercentage);
        System.out.println("Enter women percentage (0-100)");
        try {
            womenPercentage = scanner.nextInt();
            if(womenPercentage < 0 || womenPercentage > 100){
                System.out.println("Wrong quantity! The women's percentage will set to 33");
                menPercentage = 33;
            }
        }catch (NumberFormatException e){
            System.out.println("Wrong quantity! The women's percentage will set to 33");
        }
        result.add(womenPercentage);

        return result;
    }

    public static int askForCashDeskQuantity(){
        System.out.println("Enter quantity of cash desks");
        int cashDeskQuantity = 10;
        try{
            cashDeskQuantity = scanner.nextInt();
        }catch (NumberFormatException e){
            System.out.println("Wrong number! There will be a 10 cash desks created by default");
            return cashDeskQuantity;
        }
            return cashDeskQuantity;

    }
}
