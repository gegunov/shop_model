package com.getjavajob.egunov.competentum.shopmodel;

import java.util.List;

public class Men extends Customer {

    public Men(int productQuantity) {
        super(productQuantity);
    }

    @Override
    public void chooseCashDesk(List<CashDesk> cashDeskList) {
        CashDesk actual = cashDeskList.get(0);
        for(CashDesk cashDesk : cashDeskList){
            if(cashDesk.countStepsToLastCustomer() < actual.countStepsToLastCustomer()){
                actual = cashDesk;
            }
        }
        actual.addBuyerToQueue(this);
    }
}
